# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
    config = [
        {
            "label": _("Tools"),
            "items": [
                {
                    "type": "doctype",
                    "name": "HR Component Calculation",
                    "description": _("Component Calculation Tool."),
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Data Import Beta",
                    "label":"Upload Data",
                    "description": _("Data Upload Tool."),
                    "onboard": 1,
                }
            ]
        },
        {
            "label": _("Component Settings"),
            "items": [
                {
                    "type": "doctype",
                    "name": "HR Enhancement Setting",
                    "description": _("Set Leave, Overtime and Holiday Component."),
                    "onboard": 1,
                }
            ]
        }
        
    ]
    return config