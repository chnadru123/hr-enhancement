import frappe
import datetime

def validate(self,method):
    # frappe.msgprint(str(self.end_time))
    
    shift_time = datetime.datetime.strptime(self.end_time, '%H:%M:%S') - datetime.datetime.strptime(self.start_time, '%H:%M:%S')
    self.shift_time = shift_time.total_seconds()/3600
    