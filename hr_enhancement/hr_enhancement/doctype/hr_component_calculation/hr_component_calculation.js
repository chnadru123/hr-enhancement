// Copyright (c) 2020, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('HR Component Calculation', {
	refresh: function(frm) {
		// console.log(cur_frm)
		cur_frm.add_custom_button(__("Calculate Component"), function() {
			frm.trigger("calculate_component")
		})
			// }).toggleClass('btn-primary', (frm.doc.component || []).length > 0);

		cur_frm.add_custom_button(__("Get Employees"), function() {
			frm.trigger("get_employee");
			}).toggleClass('btn-primary', !(frm.doc.component || []).length);

		
		
		if(frm.doc.validate_attendance == 1 && (frm.doc.component || []).length > 0){
			cur_frm.add_custom_button(__("Get Data"), function() {
			frm.trigger("get_data");	
			}).toggleClass('btn-primary');
		}

		cur_frm.add_custom_button(__("Reset"), function() {
			frm.trigger("reset");
		}).toggleClass('btn-danger');
		


	},
	start_date: function (frm) {
		// frm.trigger("reset");
		if(frm.doc.start_date){
			
			frm.trigger("set_end_date");
			frm.set_value("component",[])
			frm.refresh_field("component")
		}
	},
	set_end_date: function(frm){
		frappe.call({
			method: 'erpnext.hr.doctype.payroll_entry.payroll_entry.get_end_date',
			args: {
				frequency: "Monthly",
				start_date: frm.doc.start_date
			},
			callback: function (r) {
				if (r.message) {
					frm.set_value('end_date', r.message.end_date);
				}
			}
		});
	},
	get_employee:function(frm){
		if(frm.doc.start_date && frm.doc.end_date){
			frm.set_value("component",[])
			frm.refresh_field("component")
			return frappe.call({
				doc: frm.doc,
				method: 'fill_employee',
				callback: function(r) {
					if (r.docs[0].component){
						// frm.save();
						frm.refresh();
					}
					
					// }
				}
			})
		}else{
			frappe.throw("Please select Start and End Date First!")
		}
		
	},
	calculate_component:function(frm){
		return frappe.call({
			doc: frm.doc,
			method: 'calculate_component',
			callback: function(r) {
				if (r.docs[0].component){
					// frm.save();
					frm.refresh();
				}
				
				// }
			}
		})
	},
	validate_attendance: function(frm){
		if(frm.doc.validate_attendance && frm.doc.component){
			frappe.call({
				method: 'validate_employee_attendance',
				args: {},
				callback: function(r) {
					render_employee_attendance(frm, r.message);
					frm.refresh()
				},
				doc: frm.doc,
				freeze: true,
				freeze_message: 'Validating Employee Attendance...'
			});
		}else{
			frm.fields_dict.attendance_detail_html.html("");
			frm.refresh();
		}
	},
	get_data:function(frm){
		return frappe.call({
			doc: frm.doc,
			method: 'get_data',
			callback: function(r) {
				if (r.docs[0].component){
					// frm.save();
					// console.log(cur_frm.custom_buttons['Calculate Component'][0])
					frm.refresh_field("component")
					cur_frm.custom_buttons['Calculate Component'].toggleClass('btn-primary');
					cur_frm.custom_buttons['Get Data'].toggleClass('btn-primary');
					
					
				}
				
				// }
			}
		})
	},
	reset:function(frm){
		frm.set_value("component",[])
		frm.set_value("branch","")
		frm.set_value("department","")
		frm.set_value("designation","")
		frm.set_value("number_of_employee",0)
		frm.set_value("start_date","")
		frm.set_value("end_date","")
		frm.set_value("validate_attendance",0)
		frm.set_value("attendance_detail_html","")
		frm.refresh_field("component","branch","department","number_of_employee","start_date","end_date","validate_attendance","attendance_detail_html")
		frm.save();
	}
});

let render_employee_attendance = function(frm, data) {
	frm.fields_dict.attendance_detail_html.html(
		frappe.render_template('employees_to_mark_attendance', {
			data: data
		})
	);
}
