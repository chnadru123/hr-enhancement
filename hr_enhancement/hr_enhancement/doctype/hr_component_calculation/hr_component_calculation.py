# -*- coding: utf-8 -*-
# Copyright (c) 2020, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _
from erpnext.hr.doctype.employee.employee import get_holiday_list_for_employee
from frappe.utils import date_diff,getdate

class HRComponentCalculation(Document): 
	def validate(self):
		for comp in self.component:
			if comp.over_time_amount > 0:
				self.create_component(comp.employee,comp.over_time_amount,frappe.db.get_value("HR Enhancement Setting",None,"overtime_component"))
			if comp.holiday_amount > 0:
				self.create_component(comp.employee,comp.holiday_amount,frappe.db.get_value("HR Enhancement Setting",None,"holiday_component"))
			if comp.absent_amount > 0:
				self.create_component(comp.employee,comp.absent_amount,frappe.db.get_value("HR Enhancement Setting",None,"leave_component"))
			if comp.bonus_amount > 0:
				self.create_component(comp.employee,comp.bonus_amount,frappe.db.get_value("HR Enhancement Setting",None,"bonus_component"))
			frappe.msgprint("Salary Component created for "+comp.employee_name)
		
	def calculate_component(self):
		# frappe.msgprint("here")
		for comp in self.component:
			flag = 0
			if comp.over_timein_hours and comp.over_time_days:
				raw_overtime = frappe.db.get_value("Employee",comp.employee,"raw_ot")
				if raw_overtime:
					over_time = self.calculate_overtime(raw_overtime,comp.per_day_pay,comp.regular_hours_per_day,comp.over_timein_hours,comp.over_time_days,comp.work_on_holiday,comp.allowed_leave,comp.absent)
					comp.over_time_amount = over_time
					flag = 1
				else:
					frappe.msgprint("Please Add Overtime amount in Employee {}".format(comp.employee))
					comp.over_time_amount = 0
					flag = 1
			if comp.holiday_working_hours and comp.work_on_holiday:
				holiday = self.calculate_holiday(comp.basic_pay,comp.normal_hours,comp.holiday_working_hours,comp.work_on_holiday,comp.per_day_pay)
				comp.holiday_amount = holiday
				flag = 1
			if comp.absent:
				absent = self.calculate_absent(comp.per_day_pay,comp.absent,comp.regular_hours_per_day)
				comp.absent_amount = absent
				flag = 1
			if flag:
				frappe.msgprint("Salary Components Calculated for <b>"+str(comp.employee_name)+"</b>")

	def get_holidays_for_employee(self,employee,start_date, end_date):
		holiday_list = get_holiday_list_for_employee(employee)
		holidays = frappe.db.sql_list('''select holiday_date from `tabHoliday`
			where
				parent=%(holiday_list)s
				and holiday_date >= %(start_date)s
				and holiday_date <= %(end_date)s''', {
					"holiday_list": holiday_list,
					"start_date": start_date,
					"end_date": end_date
				})

		holidays = [str(i) for i in holidays]

		return holidays

	def fill_employee(self):
		self.set('employees', [])
		self.set('validate_attendance',0)
		cond = ""
		for f in ['company', 'branch', 'department', 'designation']:
			if self.get(f):
				cond += " and t1." + f + " = '" + self.get(f).replace("'", "\'") + "'"
		employees = frappe.db.sql("""select distinct t1.name  as employee ,t1.employee_name,t1.shift_time,t2.salary_structure from `tabEmployee` t1, `tabSalary Structure Assignment` t2 where t1.name = t2.employee and t2.docstatus = 1 %s order by t2.from_date desc""" % cond, as_dict=True)

		if not employees:
			frappe.throw(_("No employees for the mentioned criteria"))

		for d in employees:
			holiday_num = len(self.get_holidays_for_employee(d.employee,self.start_date,self.end_date))
			# frappe.msgprint(str(holiday_num))
			total_days = date_diff(self.end_date,self.start_date) + 1
			working_day = total_days - holiday_num
			# frappe.msgprint(str(working_day))
			salary_struc = frappe.db.get_all("Salary Detail",{"parent":d.salary_structure,"parentfield":"earnings"},['salary_component','amount'])
			amount = 0
			for sal in salary_struc:
				if sal.salary_component == "Basic":
					amount = sal.amount
			# frappe.msgprint(str(amount))
			d['per_day_pay'] = amount/210
			d['basic_pay'] = amount
			d['normal_hours'] = d.shift_time * working_day	
			d['regular_hours_per_day'] = d.shift_time
			self.append('component', d)
		self.number_of_employee = len(employees)

	def calculate_overtime(self,raw_overtime,per_day,per_day_hour,ot,ot_days,holiday,allowed,absent):
		# raw_ot = ot * 1.5 * per_day
		
		raw_ot = raw_overtime
		# frappe.msgprint(str(raw_ot))
		holiday_amt = 0
		allow_amt = 0
		absent_amt = 0
		if holiday > 0:
			holiday_amt = (raw_ot/24)*holiday
		if allowed > 0:
			allow_amt = (raw_ot/24)*allowed 
		if absent > 0:
			absent_amt = (raw_ot/24)*absent 

		ot_amount = raw_ot - holiday_amt - allow_amt - absent_amt

		return ot_amount
		

	def calculate_holiday(self,basic,hrs,perday_hr,holiday,per_day):
		holiday_amt = 16.5*per_day*holiday
		return holiday_amt

	def calculate_absent(self,per_day,absent,per_day_hr):
		
		absent_amount = per_day*absent*per_day_hr
		# frappe.msgprint(str(per_day))
		# frappe.msgprint(str(absent))
		# frappe.msgprint(str(absent_amount))
		return absent_amount

	def create_component(self,employee,value,component):
		if value > 0:
			company = frappe.db.get_value('Employee', employee, 'company')
			additional_salary = frappe.new_doc('Additional Salary')
			additional_salary.employee = employee
			additional_salary.salary_component = component
			additional_salary.amount = value
			additional_salary.payroll_date = self.start_date
			additional_salary.company = company
			additional_salary.submit()

	def get_count_holidays_of_employee(self, employee):
		holiday_list = get_holiday_list_for_employee(employee)
		holidays = 0
		if holiday_list:
			days = frappe.db.sql("""select count(*) from tabHoliday where
				parent=%s and holiday_date between %s and %s""", (holiday_list,
				self.start_date, self.end_date))
			if days and days[0][0]:
				holidays = days[0][0]
		return holidays

	def get_count_employee_attendance(self, employee):
		marked_days = 0
		attendances = frappe.db.sql("""select count(*) from tabAttendance where
			employee=%s and docstatus=1 and attendance_date between %s and %s""",
			(employee, self.start_date, self.end_date))
		if attendances and attendances[0][0]:
			marked_days = attendances[0][0]
		return marked_days

	def validate_employee_attendance(self):
		employees_to_mark_attendance = []
		days_in_payroll, days_holiday, days_attendance_marked = 0, 0, 0
		for employee_detail in self.component:
			days_holiday = self.get_count_holidays_of_employee(employee_detail.employee)
			days_attendance_marked = self.get_count_employee_attendance(employee_detail.employee)
			days_in_payroll = date_diff(self.end_date, self.start_date) + 1
			if days_in_payroll > days_holiday + days_attendance_marked:
				employees_to_mark_attendance.append({
					"employee": employee_detail.employee,
					"employee_name": employee_detail.employee_name
					})
		return employees_to_mark_attendance

	def get_data(self):
		for comp in self.component:
			shift_time = frappe.db.get_value("Employee",comp.employee,"shift_time")
			holiday_list= self.get_holidays_for_employee(comp.employee,self.start_date,self.end_date)
			employee_attendance = frappe.db.get_all("Attendance",filters = [["employee","=",comp.employee],["attendance_date","between",[self.start_date,self.end_date]],["docstatus","=",1]],fields=["status","leave_type","working_hours","attendance_date"])
			# frappe.msgprint(str(employee_attendance))
			overtime,overtime_day,holidaytime,holiday_day,allowed,absent = 0.00,0.00,0.00,0.00,0.00,0.00
			frappe.msgprint("Captured data for "+comp.employee_name)
			for at in employee_attendance:
				if at.working_hours > shift_time and str(at.attendance_date) not in holiday_list:
					overtime +=  at.working_hours - shift_time
					
					overtime_day += 1
				# if str(at.attendance_date) in holiday_list:
				# frappe.msgprint(str(at.attendance_date))
				# frappe.msgprint(str(getdate(str(at.attendance_date)).weekday()))
				if str(at.attendance_date) in holiday_list and getdate(str(at.attendance_date)).weekday() != 6 and at.working_hours > 0:
					# if getdate(str(at.attendance_date)).weekday() == 6 and at.working_hours > shift_time:
					holidaytime += at.working_hours
					holiday_day += 1
				elif str(at.attendance_date) in holiday_list and getdate(str(at.attendance_date)).weekday() == 6 and at.working_hours > shift_time:
					holidaytime += at.working_hours
					holiday_day += 1
				if at.status == "Absent":
					absent += 1
				if at.status == "On Leave":
					allowed += 1
			# frappe.msgprint(str(overtime))
			comp.over_timein_hours = float(overtime)
			comp.over_time_days = overtime_day
			comp.work_on_holiday = holiday_day
			comp.holiday_working_hours = holidaytime
			comp.absent = absent
			comp.allowed_leave = allowed
		

		

