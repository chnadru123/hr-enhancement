# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in hr_enhancement/__init__.py
from hr_enhancement import __version__ as version

setup(
	name='hr_enhancement',
	version=version,
	description='Overtime,holiday,Absent calculation',
	author='Raj Tailor',
	author_email='tailorraj111@gmail.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
